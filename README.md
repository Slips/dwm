# Slips's DWM Config

Now with extra tea!

As of 02/08/22.

Included is an example xinitrc, an autostart script, my GLava rc.glsl, any scripts used by dwm, and the wallpaper I use, alongside a quickstart script for Arch users!

# INSTALLATION

In the base directory, after having installed dependencies, just `make install` as root or with your privelege escalation tool of choice.

# DEPENDENCIES:

dmenu

alacritty (Or any terminal, can be set within the config)

maim

xclip

(Optional, but not having will disable volume binds) pamixer

(Optional, but necessary for Visualizer) GLava

Included patches:

fibonacci

default tag apps

hidevacanttags

centeredwindowname

swallow

restartsig

systray

scratchpad

[bakkeby/unmanaged](https://github.com/bakkeby/patches/wiki/unmanaged)

and probably some more i forgot about.

Works best with my [dwmblocks config!](https://git.envs.net/Slips/dwmblocks)

