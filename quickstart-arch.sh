if [ $(which make) = "make not found" ]; then
	echo "!!! MAKE NOT FOUND, SCRIPT ENDING PREMATURELY !!!"
	exit 1
fi

#!/bin/sh

# HELLO
#
# Read this script in it's entirety before executing it.
# DO NOT EXECUTE SCRIPTS WITHOUT READING THEM FIRST.
#
# If you are not okay with any action taken by this script, press Q at the next question, which will kill the script.
#
# Exit this prompt by pressing q.
#
# -Slips

cat ./quickstart.sh | less

workingDir=$(pwd)
echo "PLEASE ENSURE THAT YOU ARE RUNNING THIS SCRIPT FROM WITHIN THE BASE DIRECTORY FOR DWM. THE CURRENT WORKING DIRECTORY IS: $workingDir . IS THIS THE DWM BASE DIRECTORY?"
while :
do
        read isPwdTrue
        case $isPwdTrue in
                [nN][oO]|[nN])            
			echo "Please run this script from within the dwm base directory."
			exit 0
                        ;;
                [yY][eE][sS]|[yY])
                        echo "Okay, continuing..."
                        break
                        ;;
		[qQ][uU][iI][tT]|[qQ])
			echo "Killing script..."
			exit 0
			;;
                *)
                        echo "Input not understood, please only input Y/y/N/n."
                        ;;
        esac
done
echo "Would you like me to install necessary pulseaudio packages?"
while :
do
        
        read paInstall
        case $paInstall in
                [yY][eE][sS]|[yY])
			sudo pacman -S --needed pulseaudio pulseaudio-alsa alsa-lib
                        break
                        ;;
                [nN][oO]|[nN])
                        echo "Okay, skipping..."
                        break
                        ;;
                *)
                        echo "Input not understood, please only input Y/y/N/n."
                        ;;
        esac
done
echo "Installing necessary prerequisites..."
sudo pacman -S --needed lm_sensors feh dmenu alacritty otf-font-awesome ttf-liberation maim xclip xorg-xinit pamixer picom
echo "Installing necessary prerequisites complete. Would you like me to also install Slips' dwmblocks?"
while :
do
	read dwmblocksInstall
	case $dwmblocksInstall in
		[yY][eE][sS]|[yY])
                        echo "Please give target directory:"
                        read dwmblocksDir
                        mkdir -p $dwmblocksDir
                        git clone https://git.envs.net/Slips/dwmblocks $dwmblocksDir
                        cd $dwmblocksDir
                        sudo make install
                        cd $workingDir
                        echo dwmblocks install complete.
                        break
                        ;;
		[nN][oO]|[nN])
			echo "Okay, skipping..."
			break
			;;
		*)
			echo "Input not understood, please only input Y/y/N/n."
			;;
	esac
done
cd $workingDir
sudo make install
sudo mkdir /usr/local/bin/dwmscripts
sudo cp $workingDir/goodies/scripts/* /usr/local/bin/
mkdir ~/Pictures
cp $workingDir/goodies/wallhaven-5wej77.png ~/Pictures
echo "Install completed. An example .xinitrc can be found within the goodies subdir. Would you like me to copy that to the ~/ dir?"
while :
do
        read xinitMove
        case $xinitMove in
                [yY][eE][sS]|[yY])                        
			cp $workingDir/goodies/examplexinit ~/.xinitrc
                        break
                        ;;
                [nN][oO]|[nN])
                        echo "Okay, skipping..."
                        break
                        ;;
                *)
                        echo "Input not understood, please only input Y/y/N/n."
                        ;;
        esac
done
